package controller;

import model.User;
import repository.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    private UserDAO userDAO;

    public void init() {
        userDAO = new UserDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = null;
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        try {
            user = userDAO.login(email, password);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        if (user != null) {
            HttpSession session = request.getSession();
            session.setAttribute("uid", user.getId());
            response.sendRedirect("/EcommerceProduct/ProductServlet/list");
        } else {
            request.setAttribute("message", "Wrong email or password");
            request.getRequestDispatcher("login.jsp").include(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("uid") != null) {
            boolean isLogout = Boolean.parseBoolean(request.getParameter("logout"));
            if(isLogout) {
                session.removeAttribute("uid");
                response.sendRedirect("/EcommerceProduct/LoginServlet");
                return;
            }
            response.sendRedirect("/EcommerceProduct/ProductServlet/list");
        } else {
            request.getRequestDispatcher("login.jsp").include(request, response);
        }
    }
}
