package repository;

import model.User;

import java.sql.*;

public class UserDAO {
    private Connection dbConnection;

    public UserDAO() {
        dbConnection = DBConnection.getConnection();
    }

    public User login(String email, String password) throws SQLException {
        User user = null;

        String sqlQuery = "SELECT * FROM user WHERE email = ? AND password = ? LIMIT 1";
        PreparedStatement statement = dbConnection.prepareStatement(sqlQuery);
        statement.setString(1, email);
        statement.setString(2, password);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            int id = resultSet.getInt("id");
            user = new User(id, email, password);
        }

        return user;
    }

    public User getUser(int id) throws SQLException {
        User user = null;

        String sql = "SELECT * FROM user WHERE id = ? LIMIT 1";
        PreparedStatement statement = dbConnection.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            String email = resultSet.getString("email");
            String password = resultSet.getString("password");
            user = new User(id, email, password);
        }

        return user;
    }
}
