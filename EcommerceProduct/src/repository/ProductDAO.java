package repository;

import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {
    private Connection dbConnection;

    public ProductDAO() {
        dbConnection = DBConnection.getConnection();
    }

    public boolean insertProduct(Product product) throws SQLException {
        String sql = "INSERT INTO product (name, type, price) VALUES (?, ?, ?)";

        PreparedStatement statement = dbConnection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getType());
        statement.setFloat(3, product.getPrice());

        return statement.executeUpdate() > 0;
    }

    public List<Product> listAllProducts() throws SQLException {
        List<Product> listProduct = new ArrayList<Product>();

        String sql = "SELECT * FROM product";

        Statement statement = dbConnection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            int id = resultSet.getInt("product_id");
            String name = resultSet.getString("name");
            String type = resultSet.getString("type");
            float price = resultSet.getFloat("price");

            Product product = new Product(id, name, type, price);
            listProduct.add(product);
        }

        return listProduct;
    }

    public boolean deleteProduct(Product product) throws SQLException {
        String sql = "DELETE FROM product where product_id = ?";

        PreparedStatement statement = dbConnection.prepareStatement(sql);
        statement.setInt(1, product.getId());

        return statement.executeUpdate() > 0;
    }

    public boolean updateProduct(Product product) throws SQLException {
        String sql = "UPDATE product SET name = ?, type = ?, price = ? WHERE product_id = ?";

        PreparedStatement statement = dbConnection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getType());
        statement.setFloat(3, product.getPrice());
        statement.setInt(4, product.getId());

        return statement.executeUpdate() > 0;
    }

    public Product getProduct(int id) throws SQLException {
        Product product = null;

        String sql = "SELECT * FROM product WHERE product_id = ?";

        PreparedStatement statement = dbConnection.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            String name = resultSet.getString("name");
            String type = resultSet.getString("type");
            float price = resultSet.getFloat("price");
            product = new Product(id, name, type, price);
        }

        return product;
    }
}
