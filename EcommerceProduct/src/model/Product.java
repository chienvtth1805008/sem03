package model;

public class Product {
    protected int id;
    protected String name;
    protected String type;
    protected float price;

    public Product() {
    }

    public Product(int id) {
        this.id = id;
    }

    public Product(String name, String type, float price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public Product(int id, String name, String type, float price) {
        this(name, type, price);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
