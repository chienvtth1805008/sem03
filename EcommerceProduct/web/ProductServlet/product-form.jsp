<%--
  Created by IntelliJ IDEA.
  User: Chien
  Date: 02/05/2020
  Time: 10:05 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Product Form</title>
</head>
<body>
<div>
    <h1>Products Management</h1>
    <h3>
        <a href="new">Add New Product</a>
        <a href="list">List All Products</a>
    </h3>
</div>
<div>
    <c:if test="${product != null}">
    <form action="update" method="post">
        </c:if>
        <c:if test="${product == null}">
        <form action="insert" method="post">
            </c:if>
            <table>
                <caption>
                    <h2><c:if test="${product != null}">Edit model.Product</c:if>
                        <c:if test="${product == null}">Add New model.Product</c:if></h2>
                </caption>
                <c:if test="${product != null}">
                    <input type="hidden" name="id" value="<c:out value='${product.id}' />"/></c:if>
                <tr>
                    <th>Name:</th>
                    <td><input type="text" name="name" value="<c:out value='${product.name}' />"/></td>
                </tr>
                <tr>
                    <th>Type:</th>
                    <td><input type="text" name="type" value="<c:out value='${product.type}' />"/></td>
                </tr>
                <tr>
                    <th>Price:</th>
                    <td><input type="text" name="price" value="<c:out value='${product.price}' />"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Save"/></td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
