<%--
  Created by IntelliJ IDEA.
  User: Chien
  Date: 02/05/2020
  Time: 10:26 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Product List</title>
</head>
<body>
<div>
    <h1>Products Management</h1>
    <h2>
        <a href="new">Add New Product</a>
        <a href="list">List All Products</a>
        <a href="/EcommerceProduct/LoginServlet?logout=true">Log out</a>
    </h2>
</div>
<div>
    <table>
        <caption><h2>List of Products</h2></caption>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Type</th>
            <th>Price</th>
            <th>Actions</th>
        </tr>
        <c:forEach var="product" items="${listProduct}">
            <tr>
                <td><c:out value="${product.id}"/></td>
                <td><c:out value="${product.name}"/></td>
                <td><c:out value="${product.type}"/></td>
                <td><c:out value="${product.price}"/></td>
                <td>
                    <a href="edit?id=<c:out value='${product.id}' />">Edit</a>
                    <a href="delete?id=<c:out value='${product.id}' />">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
