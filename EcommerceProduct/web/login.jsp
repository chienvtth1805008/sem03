<%--
  Created by IntelliJ IDEA.
  User: Chien
  Date: 02/05/2020
  Time: 09:18 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Login Page</title>
</head>
<body>
<div class="login_form">
    <h1 class="box">Login</h1>
    <p class="error-message"><c:out value="${message}"/></p>
    <form action="LoginServlet" method="post">
        <input class="box" type="text" name="email" placeholder="Email"><br/>
        <input class="box" type="password" name="password" placeholder="Password"><br/>
        <input class="box" type="submit" value="Login"><br/>
    </form>
</div>
</body>
</html>

