﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeNew" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeNew.svc or EmployeeNew.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEmployeeNew
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        public bool AddEmployee(employee_01 eml)
        {
            try
            {
                data.employee_01s.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            { return false; }
        }

        public bool DeleteEmployee(int idE)
        {
            try
            {
                employee_01 employeeToDelete =
                    (from employee in data.employee_01s where employee.empID == idE select employee).Single();
                data.employee_01s.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void DoWork()
        {
            throw new NotImplementedException();
        }

        public List<employee_01> GetProductList()
        {
            try
            {
                return (from employee in data.employee_01s select employee).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployee(employee_01 eml)
        {
            employee_01 employeeToModify = (from employee in data.employee_01s where employee.empID == eml.empID select employee).Single();
            employeeToModify.age = eml.age;
            employeeToModify.address = eml.address;
            employeeToModify.firstName = eml.firstName;
            employeeToModify.lastName = eml.lastName;
            data.SubmitChanges();
            return true;
        }
    }
}
