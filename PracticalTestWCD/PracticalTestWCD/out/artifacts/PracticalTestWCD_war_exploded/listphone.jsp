<%--
  Created by IntelliJ IDEA.
  User: Chien
  Date: 14/05/2020
  Time: 10:01 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>List phone</title>
</head>
<body>
<div class="container" style="text-align: center; margin-top: 50px">
    <table class="table table-striped">
        <caption><h2>List Phones</h2></caption>
        <tr>
            <th>Name</th>
            <th>Brand</th>
            <th>Price</th>
            <th>Description</th>
        </tr>
        <c:forEach var="phone" items="${phoneList}">
            <tr>
                <td><c:out value="${phone.name}"/></td>
                <td><c:out value="${phone.brand}"/></td>
                <td><c:out value="${phone.price}"/></td>
                <td><c:out value="${phone.description}"/></td>
            </tr>
        </c:forEach>
    </table>
    <button type="button" class="btn btn-light"><a href="/PracticalTestWCD">Back</a></button>

</div>
</body>
</html>
