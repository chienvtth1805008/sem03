<%--
  Created by IntelliJ IDEA.
  User: Chien
  Date: 14/05/2020
  Time: 09:35 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>AddPhone</title>
</head>
<body>
<div class="container" style="text-align: center; margin-top: 50px">
<form action="PhoneServlet" method="post">
    <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Name" required/></div>
    <div class="form-group"><select id="brand" class="form-control" name="brand">
        <option value="Apple">Apple</option>
        <option value="Samsung">Samsung</option>
        <option value="Nokia">Nokia</option>
        <option value="Other">Other</option>
    </select></div>
    <div class="form-group"><input type="number" class="form-control" name="price" placeholder="Price" required/></div>
    <div class="form-group"><input type="text" class="form-control" name="description" placeholder="Description" required/></div>

    <div class="form-group">
    <input type="submit" class="btn btn-success" value="Submit"/>
    <input type="reset" class="btn btn-secondary" value="Reset"/>
    </div>
</form>
</div>
</body>
</html>
