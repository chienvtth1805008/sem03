package controller;

import entity.Phone;
import da.GetPhone;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "PhoneServlet")
public class PhoneServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String brand = request.getParameter("brand");
        float price = Float.parseFloat(request.getParameter("price"));
        String description = request.getParameter("description");

        try {
            Phone phone = new Phone(name, brand, price, description);
            GetPhone.insertPhone(phone);
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
        }

        response.sendRedirect("/PracticalTestWCD\n");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Phone> phoneList = null;
        try {
            phoneList = GetPhone.selectAllPhone();
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
        }
        request.setAttribute("phoneList", phoneList);
        request.getRequestDispatcher("listphone.jsp").include(request, response);
    }
}
