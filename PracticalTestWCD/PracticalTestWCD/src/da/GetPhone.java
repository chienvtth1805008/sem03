package da;

import entity.Phone;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GetPhone {
    public static Boolean insertPhone(Phone phone) throws SQLException, ClassNotFoundException {
        String sql = "INSERT INTO phone (name, brand, price, description) VALUES (?, ?, ?, ?)";

        Connection connection = DBConnection.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.setString(1, phone.getName());
        statement.setString(2, phone.getBrand());
        statement.setFloat(3, phone.getPrice());
        statement.setString(4, phone.getDescription());

        return statement.executeUpdate() > 0;
    }

    public static List<Phone> selectAllPhone() throws SQLException, ClassNotFoundException {
        List<Phone> listPhone = new ArrayList<Phone>();

        String sql = "SELECT * FROM phone";

        Connection connection = DBConnection.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            String name = resultSet.getString("name");
            String brand = resultSet.getString("brand");
            float price = resultSet.getFloat("price");
            String description = resultSet.getString("description");

            Phone phone = new Phone(name, brand, price, description);
            listPhone.add(phone);
        }

        return listPhone;
    }
}
