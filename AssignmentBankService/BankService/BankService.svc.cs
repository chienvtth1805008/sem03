﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BankService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BankService.svc or BankService.svc.cs at the Solution Explorer and start debugging.
    public class BankService : IBankService
    {
        BankdataDataContext db = new BankdataDataContext();
        public bool AddTransactionHistory(TransactionHistrory transactionHistory)
        {
            try
            {
                db.TransactionHistrories.InsertOnSubmit(transactionHistory);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteTransactionHistory(int tranId)
        {
            try
            {
                TransactionHistrory transactionHistoryDelete =
                    (from transactionHistory in db.TransactionHistrories where transactionHistory.transactionId == tranId select transactionHistory).Single();
                db.TransactionHistrories.DeleteOnSubmit(transactionHistoryDelete);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<TransactionHistrory> GetTransactionHistories()
        {
            try
            {
                return (from transactionHistory in db.TransactionHistrories select transactionHistory).ToList();
            }
            catch
            {
                return null;
            }
        }
        public bool UpdateTransactionHistory(TransactionHistrory tran)
        {
            TransactionHistrory transactionToModify =
                (from tracsaction in db.TransactionHistrories where tracsaction.transactionId == tran.transactionId select tracsaction).Single();
            transactionToModify.transactionCode = tran.transactionCode;
            transactionToModify.transactionAmount = tran.transactionAmount;
            transactionToModify.transactionName = tran.transactionName;
            transactionToModify.transactionFee = tran.transactionFee;
            transactionToModify.partnerId = tran.partnerId;
            transactionToModify.customerId = tran.customerId;
            db.SubmitChanges();
            return true;
        }

        public bool AddPartner(Partner partner)
        {
            try
            {
                db.Partners.InsertOnSubmit(partner);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeletePartner(int partId)
        {
            try
            {
                Partner partnerDelete =
                    (from partner in db.Partners where partner.partnerId == partId select partner).Single();
                db.Partners.DeleteOnSubmit(partnerDelete);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Partner> GetPartners()
        {
            try
            {
                return (from partner in db.Partners select partner).ToList();
            }
            catch
            {
                return null;
            }
        }
        public bool UpdatePartner(Partner part)
        {
            Partner partnerToModify =
                (from partner in db.Partners where partner.partnerId == part.partnerId select partner).Single();
            partnerToModify.partnerCode = part.partnerCode;
            partnerToModify.partnerPassword = part.partnerPassword;
            partnerToModify.partnerBalance = part.partnerBalance;
            db.SubmitChanges();
            return true;
        }

        public bool AddCustomer(Customer customer)
        {
            try
            {
                db.Customers.InsertOnSubmit(customer);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteCustomer(int cusId)
        {
            try
            {
                Customer customerDelete =
                    (from customer in db.Customers where customer.customerId == cusId select customer).Single();
                db.Customers.DeleteOnSubmit(customerDelete);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Customer> GetCustomers()
        {
            try
            {
                return (from customer in db.Customers select customer).ToList();
            }
            catch
            {
                return null;
            }
        }
        public bool UpdateCustomer(Customer cus)
        {
            Customer customerToModify =
                (from customer in db.Customers where customer.customerId == cus.customerId select customer).Single();
            customerToModify.customerCode = cus.customerCode;
            customerToModify.cusstomerPin = cus.cusstomerPin;
            customerToModify.customerBalance = cus.customerBalance;
            db.SubmitChanges();
            return true;
        }
    }
}
